package test.java;

import main.java.Cafe;
import main.java.Coffee;
import main.java.CoffeeType;
import org.junit.Assert;
import org.junit.Test;

public class CafeTest {

    @Test
    public void canBrewEspresso() {
        //Given
        Cafe cafe = new Cafe();
        cafe.restockBeans(7);

        //When
        Coffee coffee = cafe.brew(CoffeeType.ESPRESSO);

        //Then
        Assert.assertEquals(CoffeeType.ESPRESSO, coffee.getType());
        Assert.assertEquals(7, coffee.getBeans());
        Assert.assertEquals(0, coffee.getMilk());
    }

    @Test
    public void canBrewLatte() {
        Cafe cafe = new Cafe();
        cafe.restockBeans(7);
        cafe.restockMilk(277);

        Coffee coffee = cafe.brew(CoffeeType.LATTE);

        Assert.assertEquals("Wrong coffee type", CoffeeType.LATTE, coffee.getType());
        Assert.assertEquals(7, coffee.getBeans());
        Assert.assertEquals(277, coffee.getMilk());
    }

}
