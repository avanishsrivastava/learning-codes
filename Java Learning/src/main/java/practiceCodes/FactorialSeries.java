package practiceCodes;

import java.util.Scanner;

public class FactorialSeries {

    public static void main(String ar[]) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        System.out.printf("Factorial of %d = %d", num,  +getFactorial(num));
    }

    private static int getFactorial(int num) {
        int fact = 1;
        fact = ((num > 1) ? num * getFactorial(--num) : 1) ;
//        if(num > 1)
//            fact = num * getFactorial(--num);
        return fact;
    }

}
