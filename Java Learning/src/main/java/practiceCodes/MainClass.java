package practiceCodes;

public class MainClass {

    public MainClass() {
        System.out.println("printing default constructor");
    }

    public MainClass(int i, int j) {
        System.out.println("Sum of numbers: " + (i+j));
    }

    public void printSum() {
        System.out.println("printing");
    }
}
