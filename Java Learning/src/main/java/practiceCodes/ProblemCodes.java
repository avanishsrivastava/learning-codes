package practiceCodes;

import org.testng.annotations.Test;

import java.util.Scanner;

public class ProblemCodes {

    @Test
    public void reverseString() {
        Scanner sc = new Scanner(System.in);
        String statement = sc.nextLine();
        System.out.println(statement);
        String[] reverse = statement.split(" ");
        for (int i = reverse.length - 1; i >= 0; i--) {
            System.out.print(reverse[i] + " ");
        }
    }

    @Test
    public void extractNumFromString() {
        String str = "asnfv643mghl578mnff";
        str = str.replaceAll("[^0-9]", "");
        System.out.println(str);
        int sum = 0;
        for (int i = 0; i < str.length(); i++) {
            sum += Character.getNumericValue(str.charAt(i));
        }
        System.out.println(sum);
    }


    @Test
    public void testConstructors() {
        SubClass sub = new SubClass(10, 20);
    }

    @Test
    public void checkOccurrenceOfCharacter() {
        String str = "avnavjafm,afkmasfaf";
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'a')
                count++;
        }
        System.out.println(count);
    }

    @Test
    public void checkOccurrenceOfWords() {
        String str = "My Name is Name is Name is Name is Avanish";
        String commonWord = "Name";
        String[] parts = str.split(" ");
        int count = 0;
        for (int i = 0; i < parts.length; i++) {
            if (parts[i].equals(commonWord))
                count++;
        }
        System.out.println(count);
    }

    @Test
    public void checkDuplicateWordsWithCounts() {
        String str = "My Name is Name is Name is Name is Avanish Manish Avanish Kumar My is Name";
        String[] parts = str.split(" ");
        for (int i = 0; i < parts.length - 1; i++) {
            if (!parts[i].equals("")) {
                String word = parts[i];
                int count = 1;
                for (int j = i + 1; j < parts.length; j++) {
                    if (parts[j].equals(word)) {
                        count++;
                        parts[j] = "";
                    }
                }
                System.out.println(word + ": " + count + " times");

            }
        }
    }

    @Test
    public void checkPrimeNumber() {
        for(int num= 2; num<=100; num++) {
            int flag = 0;
            for (int i = 2; i <= num / 2; i++) {
                if (num % i == 0) {
//                    System.out.println(num + " is not a prime number");
                    flag = 1;
                    break;
                }
            }
            if (flag == 0)
                System.out.print(num + ", ");
        }
    }

    @Test
    public void checkDuplicateCharactersWithCounts() {
        String str = "AvanishSrivastava";
        for (int i = 0; i < str.length(); i++) {

        }
    }
}
