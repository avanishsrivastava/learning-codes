package practiceCodes;

import java.util.Scanner;

public class StringReverse {

    public static String FirstReverse(String str) {
        for(int i= 0; i< str.length(); i++) {
            int num = ((int) str.charAt(i));
            str.replace(str.charAt(i), (char)(num+1));
        }

        return str;

    }

    public static void main (String[] args) {
        // keep this function call here
        Scanner s = new Scanner(System.in);
        System.out.print(FirstReverse(s.nextLine()));
    }

}

