package practiceCodes;

import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadDataFromCSV {

    @Test
    public void readCSV() {
        String filePath = "./Data.csv";
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String inValue = null;
            Pattern pattern = Pattern.compile("[aeiou]$");
            reader.readLine();
            while ((inValue = reader.readLine()) != null) {
                String[] splittedData = inValue.split(",");
                Matcher matcher = pattern.matcher(splittedData[1]);
                if (splittedData[1].length() > 5 && matcher.find()) {
                    if (Integer.parseInt(splittedData[2]) % 2 != 0) {
                        System.out.println(splittedData[0] + "," + splittedData[1] + "," + splittedData[2]);
                    }

                }

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    public boolean matchPattern(String value, String regexPattern) {
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(value);
        return matcher.find();
    }

    @Test
    public void testMatchPattern() {
        String filePath = "./Data.csv";
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String inValue = null;
            String regexPattern = "[aeiou]$";
            reader.readLine();
            while ((inValue = reader.readLine()) != null) {
                String[] splittedData = inValue.split(",");
                if (splittedData[1].length() > 5 && matchPattern(splittedData[1], regexPattern)) {
                    if (Integer.parseInt(splittedData[2]) % 2 != 0) {
                        System.out.println(splittedData[0] + "," + splittedData[1] + "," + splittedData[2]);
                    }

                }

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }
}