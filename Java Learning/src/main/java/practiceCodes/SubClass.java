package practiceCodes;

public class SubClass extends MainClass {

    public SubClass() {
        super();
        System.out.println("Printing sub class default constructor");
    }

    public SubClass(int i, int j) {
        super(i, j);
    }

}


