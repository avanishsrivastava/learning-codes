package inputOutputStreamWithJavaNio;

import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public class ZipFileSystemDemo {

    @Test
    public void testZipFileSystem() {
        String[] data = {"Line 1",
                "Line 2 2",
                "Line 3 3 3",
                "Line 4 4 4 4"};

        try (FileSystem zipFS = openZip(Paths.get(System.getProperty("user.dir"), "\\src\\inputOutputStreamWithJavaNio", "my data new.zip"))) {
            copyToZip(zipFS);
            writeToZipFile(zipFS, data);
            writeToZipFile1(zipFS, data);
        } catch (Exception e) {
            System.out.println(e.getClass().getSimpleName() + "-" + e.getMessage());
        }
    }

    private static FileSystem openZip(Path zipPath) throws IOException, URISyntaxException {
        Map<String, String> providerProps = new HashMap<>();
        providerProps.put("create", "true");

        URI zipURI = new URI("jar:file", zipPath.toUri().getPath(), null);
        FileSystem zipFS = FileSystems.newFileSystem(zipURI, providerProps);

        return zipFS;
    }

    private static void copyToZip(FileSystem zipFS) throws IOException {
        Path sourceFile = Paths.get(System.getProperty("user.dir"), "\\src\\inputOutputStreamWithJavaIO", "WriterFile.txt");
        //System.out.println(FileSystems.getDefault().getPath("\\src\\inputOutputStreamWithJavaIO\\WriterFile.txt"));
//        Path sourceFile = FileSystems.getDefault().getPath("\\src\\inputOutputStreamWithJavaIO\\WriterFile.txt");
        Path destFile = zipFS.getPath("\\new Writer File.txt");

        Files.copy(sourceFile, destFile, StandardCopyOption.REPLACE_EXISTING);
    }

    private static void writeToZipFile(FileSystem fileSystem, String[] data) {
        try (BufferedWriter writer = Files.newBufferedWriter(fileSystem.getPath("/new file1.txt"))) {
            for (String d : data) {
                writer.write(d);
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void writeToZipFile1(FileSystem fileSystem, String[] data) throws IOException {
        Files.write(fileSystem.getPath("./new File2.txt"), Arrays.asList(data), Charset.defaultCharset(), StandardOpenOption.CREATE);
    }

}
