package inputOutputStreamWithJavaNio;

import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class JavaNioDemo {

    @Test
    public void testReadFromFile() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(System.getProperty("user.dir"), "\\src\\inputOutputStreamWithJavaIO\\WriterFile.txt"));
        for(String str : lines) {
            System.out.println(str);
        }
    }


}
