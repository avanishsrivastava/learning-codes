package apiAutomation;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

public class FirstAPITest {

    @Test
    public void testAPI() {
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";
        RequestSpecification httpRequest = RestAssured.given();
        Response httpResponse = httpRequest.request(Method.GET, "/Delhi");
        int statusCode = httpResponse.getStatusCode();
        System.out.println(statusCode);
        String statusLine = httpResponse.getStatusLine();
        System.out.println(statusLine);
//        String responseBody = httpResponse.getBody().asString();
//        System.out.println(responseBody);
    }

    @Test
    public void testGetAccountAPI() {
        RestAssured.baseURI = "http://testc-pub-web0a01.test.aus.netspend.net:8081/fluidapi/api-ui/docs";
    }
}
