package inputOutputStreamWithJavaIO;

import org.testng.annotations.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class StreamsDemo {

    @Test
    public static void testReaderWithoutTryResource() {
        char[] buff = new char[8];
        int length;
        Reader reader = null;
        try {
            reader = Helper.openReader(System.getProperty("user.dir") + "\\src\\inputOutputStreamWithJavaIO\\inputOutputStreams\\ReaderFile.txt");
            while ((length = reader.read(buff)) > 0) {
                System.out.println("\nLength: " + length);
                for (int i = 0; i < length; i++) {
                    System.out.print(buff[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Test
    public void testReaderTryWithResources() {
        char[] buff = new char[8];
        int length;
        try (Reader reader = Helper.openReader(System.getProperty("user.dir") + "\\src\\inputOutputStreamWithJavaIO\\inputOutputStreams\\ReaderFile.txt")) {
            while ((length = reader.read(buff)) > 0) {
                System.out.println("\nLength: " + length);
                for (int i = 0; i < length; i++) {
                    System.out.print(buff[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testTryWithResourcesMulti() {
        char[] buff = new char[8];
        int length;
        try (Reader reader = Helper.openReader(System.getProperty("user.dir") + "\\src\\inputOutputStreamWithJavaIO\\inputOutputStreams\\ReaderFile.txt");
             Writer writer = Helper.openWriter(System.getProperty("user.dir") + "\\src\\inputOutputStreamWithJavaIO\\inputOutputStreams\\WriterFile.txt")) {
            while ((length = reader.read(buff)) > 0) {
                writer.write(buff, 0, length);
            }
            System.out.println("File copied successfully");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void doCloseThings() {
        try (MyAutoClosable my = new MyAutoClosable()) {
            my.testSomething();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            for (Throwable t : e.getSuppressed()) {
                System.out.println("Suppressed exceptions: " + t.getMessage());
            }
        }
    }

}