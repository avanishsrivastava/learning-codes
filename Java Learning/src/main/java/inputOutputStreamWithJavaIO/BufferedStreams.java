package inputOutputStreamWithJavaIO;

import org.testng.annotations.Test;

import java.io.*;

public class BufferedStreams {

    @Test
    public void testBufferedReader() {
        try (BufferedReader reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "//src//inputOutputStreamWithJavaIO//ReaderFile.txt"))) {
            int intVal;
            while ((intVal = reader.read()) > 0) {
                char charVal = (char) intVal;
                System.out.print(charVal);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testBufferedWriter() {
        String[] data = {"Line 1",
                "Line 2 2",
                "Line 3 3 3",
                "Line 4 4 4 4"};
        try (BufferedReader reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "//src//inputOutputStreamWithJavaIO//ReaderFile.txt"));
             BufferedWriter writer = new BufferedWriter(new FileWriter(System.getProperty("user.dir") + "//src//inputOutputStreamWithJavaIO//WriterFile.txt"))) {
            String inValue;
            while ((inValue = reader.readLine()) != null) {
                System.out.println(inValue);
                writer.write(inValue);
                writer.newLine();
            }
            for (String d : data) {
                writer.write(d);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
