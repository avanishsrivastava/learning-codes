package inputOutputStreamWithJavaIO;

public class MyAutoClosable implements AutoCloseable {

    public void testSomething() throws Exception {
        throw new Exception("Exception from testSomething method");
//        System.out.println("Printing something");
    }

    @Override
    public void close() throws Exception {
        throw new Exception("Exception created for close method");
//        System.out.println("Exception from close method");
    }
}
