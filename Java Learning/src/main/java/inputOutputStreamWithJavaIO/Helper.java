package inputOutputStreamWithJavaIO;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Helper {

    public static Reader openReader(String filename) throws IOException {
        return Files.newBufferedReader(Paths.get(filename));
    }

    public static Writer openWriter(String fileName) throws IOException {
        return Files.newBufferedWriter(Paths.get(fileName));
    }
}
