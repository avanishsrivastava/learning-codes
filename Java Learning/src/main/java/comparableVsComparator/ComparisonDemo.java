package comparableVsComparator;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ComparisonDemo {

    @Test
    public void doSimpleSorting() {
        int[] intArray = {1, 2, 5, 9, 2, 0, 3};
        Arrays.sort(intArray);
        System.out.println(Arrays.toString(intArray));

        String[] strArray = {"A", "B", "G", "E", "C", "D", "F",};
        Arrays.sort(strArray);
        System.out.println(Arrays.toString(strArray));

        List<String> stringList = new ArrayList<>();
        stringList.add("A");
        stringList.add("G");
        stringList.add("C");
        stringList.add("B");
        stringList.add("Q");

        Collections.sort(stringList);
        for (String str : stringList) {
            System.out.print(" " + str);
        }
    }

    @Test
    public void doObjectSorting() {
//        EmployeeWithComparable[] empArr = new EmployeeWithComparable[4];
//        empArr[0] = new EmployeeWithComparable(1, "Avanish", 28, 25000);
//        empArr[1] = new EmployeeWithComparable(4, "Pankaj", 30, 40000);
//        empArr[2] = new EmployeeWithComparable(3, "Anand", 29, 30000);
//        empArr[3] = new EmployeeWithComparable(2, "Manish", 27, 35000);
//
//        Arrays.sort(empArr);
//        System.out.println("\n" +Arrays.toString(empArr));

        EmployeeWithComparator[] empArr2 = new EmployeeWithComparator[5];
        empArr2[0] = new EmployeeWithComparator(1, "Rupesh", 28, 25000);
        empArr2[1] = new EmployeeWithComparator(4, "Pankaj", 30, 40000);
        empArr2[2] = new EmployeeWithComparator(3, "Anand", 29, 30000);
        empArr2[3] = new EmployeeWithComparator(2, "Manish", 27, 35000);
        empArr2[4] = new EmployeeWithComparator(1, "Avanish", 37, 36000);

        Arrays.sort(empArr2, EmployeeWithComparator.AgeComparator);
        System.out.println("Sorting on the basis of age \n" + Arrays.toString(empArr2));

        Arrays.sort(empArr2, EmployeeWithComparator.SalaryComparator);
        System.out.println("Sorting on the basis of salary \n" + Arrays.toString(empArr2));

        Arrays.sort(empArr2, EmployeeWithComparator.NameComparator);
        System.out.println("Sorting on the basis of name \n" + Arrays.toString(empArr2));

        Arrays.sort(empArr2, EmployeeWithComparator.IdAndNameComparator);
        System.out.println("Sorting on the basis of ID & Name \n" +Arrays.toString(empArr2));

    }

}
