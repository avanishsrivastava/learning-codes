package comparableVsComparator;

import java.util.Comparator;

public class EmployeeWithComparator {
    private int id;
    private String name;
    private int age;
    private int salary;

    public EmployeeWithComparator(int id, String name, int age, int salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "EmployeeWithComparator{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }

    public static Comparator<EmployeeWithComparator> SalaryComparator = new Comparator<EmployeeWithComparator>() {
        @Override
        public int compare(EmployeeWithComparator emp1, EmployeeWithComparator emp2) {
            return (int) (emp1.getSalary() - emp2.getSalary());
        }
    };

    public static Comparator<EmployeeWithComparator> AgeComparator = new Comparator<EmployeeWithComparator>() {
        @Override
        public int compare(EmployeeWithComparator emp1, EmployeeWithComparator emp2) {
            return (int) (emp1.getAge() - emp2.getAge());
        }
    };

    public static Comparator<EmployeeWithComparator> NameComparator = new Comparator<EmployeeWithComparator>() {
        @Override
        public int compare(EmployeeWithComparator emp1, EmployeeWithComparator emp2) {
            return emp1.getName().compareTo(emp2.getName());
        }
    };

    public static Comparator<EmployeeWithComparator> IdAndNameComparator = new Comparator<EmployeeWithComparator>() {
        @Override
        public int compare(EmployeeWithComparator emp1, EmployeeWithComparator emp2) {
            int flag = emp1.getId() - emp2.getId();
//            return flag = ((flag == 0) ? emp1.getName().compareTo(emp2.getName()) : 0);
            if(flag == 0)
                flag = emp1.getName().compareTo(emp2.getName());
            return flag;
        }
    };

}
