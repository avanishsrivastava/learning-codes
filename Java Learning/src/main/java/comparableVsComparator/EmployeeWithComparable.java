package comparableVsComparator;

public class EmployeeWithComparable implements Comparable<EmployeeWithComparable> {

    private int id;
    private String name;
    private int age;
    private int salary;

    public EmployeeWithComparable(int id, String name, int age, int salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "EmployeeWithComparable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }

    @Override
    public int compareTo(EmployeeWithComparable emp) {
        return (this.id - emp.id);
    }
}
