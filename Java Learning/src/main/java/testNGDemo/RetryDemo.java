package testNGDemo;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RetryDemo {

    private int count = 1;

    @Test(retryAnalyzer = MyRetry.class)
    public void testRetry() {
        if(count < 4 ) {
            System.out.println("Failed Test Case");
            count++;
            Assert.fail();
        }
        else
            System.out.println("Passed Test Case");
    }
}
