package testNGDemo;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.List;

public class RunTestNG {

    public static void main(String ar[]) {
        XmlSuite suite = new XmlSuite();
        suite.setName("XML Test");

        XmlTest test = new XmlTest(suite);
        test.setName("New Test");

        List<XmlClass> classes = new ArrayList<>();
        classes.add(new XmlClass("testNGDemo.RetryDemo"));

        test.setXmlClasses(classes);

        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        TestNG testng = new TestNG();
        testng.setXmlSuites(suites);
        testng.run();

    }
}
