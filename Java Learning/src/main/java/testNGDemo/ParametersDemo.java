package testNGDemo;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParametersDemo {

    @Parameters({"firstName", "lastName"})
    @Test
    public void doParameterization(@Optional("First") String firstName, @Optional("Last") String lastName) {
        System.out.println(firstName + "-" + lastName);
    }


    @DataProvider(name = "names data")
    public Object[][] createData() {
        return new Object[][]{
                {"Avanish", new Integer(25)},
                {"Rohit", new Integer(37)}
        };
    }

    @Test(dataProvider = "names data", dataProviderClass = ParametersDemo.class)
    public void testDataProvider(String name, int age) {
        System.out.println(name + " - " + age);

    }
}
