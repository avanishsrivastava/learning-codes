package testNGDemo;

import org.testng.annotations.Test;

public class DependencyDemo {

    //Soft Dependency
    @Test(alwaysRun = true)
    public void testSoftDependency() {
        System.out.println("This method should always run");
    }

    //Hard Dependency
    @Test(alwaysRun = true, dependsOnMethods = {"secondTest"})
    public void firstTest() {
        System.out.println("First Method");
    }

    @Test(dependsOnMethods = {"thirdTest"})
    public void secondTest() {
        System.out.println("Second Method");
//        System.out.println(10/0);
    }

    @Test(priority = 4)
    public void thirdTest() {
        System.out.println("Third Method");
    }

    @Test(priority = 1)
    public void testOne() {
        System.out.println("Method one");
    }

    @Test(priority = 2)
    public void testTwo() {
        System.out.println("Method two");
    }

    @Test(priority = 3, enabled = false)
    public void testThree() {
        System.out.println("Method three");
    }
}
