package testNGDemo;

import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

@Ignore
//@Test
public class IgnoreTest {

    @Test(threadPoolSize = 3, invocationCount = 10, timeOut = 10000)
    public void thirdTest() {
        System.out.println("Third Method");
    }

    @Test
    public void testOne() {
        System.out.println("Method one");
    }

    @Test
    public void testTwo() {
        System.out.println("Method two");
    }

    @Test
    public void testThree() {
        System.out.println("Method three");
    }

    @Test(threadPoolSize = 3, invocationCount = 10, timeOut = 10000)
    public void testServer() {
        System.out.println("Testing Server");
    }
}