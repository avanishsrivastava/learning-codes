package testNGDemo;

import org.testng.annotations.Test;

public class WebTest {

    private int num_of_times;

    public WebTest(int num_of_times) {
        this.num_of_times = num_of_times;
    }

    @Test
    public void testMultipleTimes() {
        while (num_of_times > 0) {
            System.out.println("Printing " + num_of_times--);

        }
    }
}
