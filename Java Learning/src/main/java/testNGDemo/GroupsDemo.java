package testNGDemo;

import org.testng.annotations.Test;

//@Test(groups = {"brokenTest"})
public class GroupsDemo {

    @Test(groups = {"funTest"})
    public void includedGroupTest() {
        System.out.println("Method defined in included group");
    }

    @Test(groups = {"funTestNew"})
    public void includedGroupTestNew() {
        System.out.println("Method defined in included group new");
    }

    @Test(groups = {"failedTest"})
    public void excludeGroupTest() {
        System.out.println("Method defined in excluded group");
    }

    @Test(groups = {"failedTestNew"})
    public void excludeGroupTestNew() {
        System.out.println("Method defined in excluded group new");
    }

}
