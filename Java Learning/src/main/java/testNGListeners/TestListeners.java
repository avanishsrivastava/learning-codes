package testNGListeners;

import org.testng.*;
import org.testng.xml.XmlSuite;

import java.util.List;

import static org.testng.Reporter.log;

public class TestListeners implements ITestListener, ISuiteListener, IReporter {

    @Override
    public void onTestStart(ITestResult iTestResult) {
        log("Test started: " + iTestResult.getTestName());
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        log("Test completed successfully: " + iTestResult.getTestName());
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        log("Test failed: " + iTestResult.getTestName());
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        log("Test skipped: " + iTestResult.getTestName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        log("Test failed but within success percentage: " + iTestResult.getTestName());
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        log("About to begin test execution: " + iTestContext.getName());
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        log("About to end test execution: " + iTestContext.getName(), true);
    }

    @Override
    public void onStart(ISuite iSuite) {
        log("About to begin suite execution: " + iSuite.getName(), true);
    }

    @Override
    public void onFinish(ISuite iSuite) {
        log("About to end suite execution: " + iSuite.getName(), true);
    }

    @Override
    public void generateReport(List<XmlSuite> list, List<ISuite> list1, String s) {

    }
}
