package testNGListeners;

import org.testng.annotations.*;

class TestListenerDemo {


    @BeforeSuite
    public void beforeSuite() {
        System.out.println("Before Suite is running");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("After Suite is running");
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("Before class is running");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("After class is running");
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("Before Test is running");
    }

    @AfterTest
    public void afterTest() {
        System.out.println("After Test is running");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Before method is running");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("After method is running");
    }

    @Test
    public void thirdTest() {
        System.out.println("Third Test");
    }

    @Test
    public void firstTest() {
        System.out.println("First Test");
    }

    @Test
    public void secondTest() {
        System.out.println("Second Test");
    }



}

