package stringFormatingRegex;

import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringJoinerDemo {

    @Test
    public void showStringJoiner() {
        StringJoiner str = new StringJoiner(", ", "{", "}");
        str.add("Alpha");
        str.add("Beta");
        str.add("Gamma");
        str.add("Avanish").add("Manish").add("Anand");
        System.out.println(str.toString());
        StringJoiner str1 = new StringJoiner(", ");
        str1.add("Alpha");
        str1.add("Beta");
        str1.add("Gamma");
        str1.add("Avanish").add("Manish").add("Anand");
        System.out.println(str.merge(str1).toString());
    }

    @Test
    public void showStringJoinerWithEmptyValue() {
        StringJoiner str = new StringJoiner(", ");
        str.setEmptyValue("EMPTY");
        System.out.println(str.toString());
        str = new StringJoiner(", ", "{", "}");
        str.setEmptyValue("EMPTY");
        System.out.println(str.toString());
    }

    @Test
    public void testStringFormatter() {
        int num1 = 10, num2 = 15, num3 = 12, num4 = 35;

        //Concatenation vs formatting
        String str1 = "The ages are " + num1 + ", " + num2 + ", " + num3 + " and " + num4;
        System.out.println(str1);
        String str2 = String.format("The ages are %d, %d, %d and %d", num1, num2, num3, num4);
        System.out.println(str2);

        //Common format conversions
        System.out.printf("%d %o %xX %f %e\n", 32, 32, 32, 123.00, 123.00000);

        //Format Flag: #
        System.out.printf("%d %#o %#x %f %E\n", 32, 32, 32, 123.00, 123.00000);

        System.out.printf("W: %d U: %d\n", 5, 235);
        System.out.printf("W: %d U: %d\n", 463, 23);

        //Format flags: Space counts
        System.out.printf("W: %4d U: %4d\n", 5, 235);
        System.out.printf("W: %4d U: %4d\n", 463, 23);

        //Format flags: 0 & -
        System.out.printf("W: %04d U: %04d\n", 5, 235);
        System.out.printf("W: %04d U: %04d\n", 463, 23);
        System.out.printf("W: %-4d U: %-4d\n", 5, 235);
        System.out.printf("W: %-4d U: %-4d\n", 463, 23);

        //Format Flag: ,
        System.out.printf("%,d\n", 123456789);
        System.out.printf("%,.2f\n", 123456789.128465);

        //Format flags: space, + &  (
        System.out.printf("%d\n", 123);
        System.out.printf("%d\n", -456);

        System.out.printf("% d\n", 123);
        System.out.printf("% d\n", -456);

        System.out.printf("%+d\n", 123);
        System.out.printf("%+d\n", -456);

        System.out.printf("%(d\n", 123);
        System.out.printf("%(d\n", -456);
        System.out.printf("% (d\n", 123);

        //Argument Index
        System.out.printf("%d %d %d\n", 100, 200, 300);
        System.out.printf("%3$d %1$d %2$d\n", 100, 200, 300);

    }

    @Test
    public void doWriteUsingFormatter() throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Paths.get("myfileNew.txt"));
        try(Formatter format = new Formatter(writer)) {
            format.format("The ages are %d %d %d %d", 10, 20, 30, 40);
        }
    }

    @Test
    public void verifyRegularExpressions() {
        String str = "apple, apple and orange please";
        System.out.println(str);
//        String str2 = str.replaceAll("ple[, ]", "ricot,");
        String str2 = str.replaceAll("ple\\b", "ricot");
        System.out.println(str2);
        String[] parts = str.split("\\b");
        for(String part: parts) {
            if(part.matches("\\w+"))
                System.out.println(part);
        }
    }

    @Test
    public void verifyRegularExpressionsUsingPattern() {
        String str = "apple, apple and orange please";
        System.out.println(str);
        Pattern pattern = Pattern.compile("\\w+");
        Matcher matcher = pattern.matcher(str);
        while(matcher.find())
            System.out.println(matcher.group());

        String strNew = "Null";
        System.out.println(strNew);
    }
}
