package utilities;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.IOException;

public class PracticeCodes {

    @Parameters({"input"})
    @Test
    public void checkNumericValue(String input) {
        try {
            Integer.parseInt(input);
            System.out.println(input + " is a numeric value");
        }
        catch (Exception e) {
            System.err.println("Error :" + e.getMessage());
            System.out.println(input + " is not a numeric value");
        }
    }

    @Test
    public void runExternalApp() {
        Runtime run = Runtime.getRuntime();

        try {
            run.exec("notepad.exe");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
