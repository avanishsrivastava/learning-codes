package enumClassDemo;

public class EnumDemo {

    public static void main(String ar[]) {

        Level level = Level.MEDIUM;
        System.out.println(level);

//        if (level == Level.HIGH) {
//            level = Level.MEDIUM;
//            System.out.println(level.toString());
//        } else if (level == Level.MEDIUM) {
//            level = Level.LOW;
//            System.out.println(level.toString());
//        }
//        else {
//            level = Level.HIGH;
//            System.out.println(level.toString());
//        }
//
//        for(Level lev : Level.values()) {
//            System.out.println(lev);
//        }

        System.out.println(Level.valueOf(("High").toUpperCase()));

        LevelWithConstructor level1 = LevelWithConstructor.HIGH;
        System.out.println("Level: " +level1);
        System.out.println("Level Code: " +level1.getLevelCode());

        LevelWithAbstract level2 = LevelWithAbstract.HIGH;
        System.out.println(level2.asLowerCase());

    }
}
