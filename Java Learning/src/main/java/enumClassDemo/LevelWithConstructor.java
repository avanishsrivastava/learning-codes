package enumClassDemo;

public enum LevelWithConstructor {

    HIGH (3),
    MEDIUM (2),
    LOW (1)
    ;

    private final int levelCode;

    private LevelWithConstructor(int levelCode) {
        this.levelCode = levelCode;
    }

    public int getLevelCode() {
        return levelCode;
    }
}
