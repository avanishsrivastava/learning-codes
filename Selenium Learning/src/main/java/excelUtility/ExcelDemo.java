package excelUtility;

import java.io.IOException;

public class ExcelDemo {

    public static void main(String ar[]) throws IOException {

        String filePath = ".//Books Data.xls";
        ExcelUtility excelUtility = new ExcelUtility();
        excelUtility.writeToExcel(excelUtility.getExcelData(), filePath);
        System.out.println("Excel file created successfully.");

        excelUtility.readExcel(filePath);

    }

}
