package excelUtility;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import utility.ExcelData;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelUtility {
    private Workbook workbook;
    private Sheet sheet;
    private List<ExcelData> excelData;
    private Row row;
    private Cell cell;

    public List<ExcelData> getExcelData() {
        excelData = new ArrayList<>();
        excelData.add(new ExcelData("Selenium WebDriver", "Rex Allen", 449, "First"));
        excelData.add(new ExcelData("Raavan", "Winson Ng", 548.37, "Second"));
        excelData.add(new ExcelData("Excel Learning", "Jackson", 548.54, "Third"));
        excelData.add(new ExcelData("PowerPoint Learning", "Jackson", 348.54, "Second"));
        excelData.add(new ExcelData("Word Learning", "Jackson", 448.54, "Third"));
        return excelData;
    }

    private void writeData(ExcelData excelData, Row row) {
        cell = row.createCell(0);
        cell.setCellValue(excelData.getTitle());

        cell = row.createCell(1);
        cell.setCellValue(excelData.getAuthor());

        cell = row.createCell(2);
        cell.setCellValue(excelData.getPrice());

        cell = row.createCell(3);
        cell.setCellValue(excelData.getEdition());

    }

    public void writeToExcel(List<ExcelData> excelData, String excelFilePath) {
        if (excelFilePath.endsWith("xlsx"))
            workbook = new XSSFWorkbook();
        else if (excelFilePath.endsWith("xls"))
            workbook = new HSSFWorkbook();
        else
            throw new IllegalArgumentException("The specified file is not excel file");

        sheet = workbook.createSheet("Books Data");
        createHeaderRow(sheet);
        int rownum = 1;

        for (ExcelData data : excelData) {
            Row row = sheet.createRow(rownum++);
            writeData(data, row);
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createHeaderRow(Sheet sheet) {
        this.sheet = sheet;
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Title");
        cell = row.createCell(1);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Author");
        cell = row.createCell(2);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Price");
        cell = row.createCell(3);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Edition");

    }

    public void readExcel(String excelFilePath) throws IOException {

        FileInputStream file = new FileInputStream(excelFilePath);
        if (excelFilePath.endsWith("xlsx"))
            workbook = new XSSFWorkbook(file);
        else if (excelFilePath.endsWith("xls"))
            workbook = new HSSFWorkbook(file);
        else
            throw new IllegalArgumentException("The specified file is not excel file");
        sheet = workbook.getSheetAt(workbook.getSheetIndex("Books Data"));

        int rowCount = sheet.getLastRowNum() + 1;
        for (int i = 0; i < rowCount; i++) {
            row = sheet.getRow(i);
            int columnCount = row.getLastCellNum();
            for (int j = 0; j < columnCount; j++) {
                cell = row.getCell(j);
                CellType cellFormat = cell.getCellTypeEnum();
                switch (cellFormat) {
                    case STRING:
                        System.out.print(cell.getStringCellValue() + "\t\t\t");
                        break;
                    case NUMERIC:
                        System.out.print(cell.getNumericCellValue() + "\t\t\t");
                        break;
                    case FORMULA:
                        System.out.print(cell.getCellFormula() + "\t\t\t");
                        break;
                    case BOOLEAN:
                        System.out.print(cell.getBooleanCellValue() + "\t\t\t");
                        break;
                    default:
                        System.out.print("Invalid data format");
                        break;
                }
            }
            System.out.println();
        }

    }

}
