package databaseDemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {

    //Connection URL Default template -----> jdbc:oracle:<driver-type>:@<hostname>:<portnumber>:<SID>/<service-name>
    private static String dbURLOracle = "jdbc:oracle:thin:@testdb0d-scan.test.aus.netspend.net:1521/QALZ_ADHOC";
    private static String userNameOracle = "BI";
    private static String passwordOracle = "Dunk!nD0nuts";

    private static String dbURLMYSQL = "jdbc:mysql://corp-db-mysql03.hq.netspend.com:3306/jira7db?serverTimezone=UTC";
    private static String userNameMYSQL = "readuser";
    private static String passwordMYSQL = "MysqlRead";

    public static Connection getConnection(DBType dbType) throws SQLException {
        switch (dbType) {
            case ORACLE:
                return DriverManager.getConnection(dbURLOracle, userNameOracle, passwordOracle);
            case MYSQL:
                return DriverManager.getConnection(dbURLMYSQL, userNameMYSQL, passwordMYSQL);
            default:
                return null;
        }
    }

    public static void showErrorMessage(SQLException e) {
        System.err.println("Error : " + e.getErrorCode());
        System.err.println("Error message : " + e.getMessage());
    }

}
