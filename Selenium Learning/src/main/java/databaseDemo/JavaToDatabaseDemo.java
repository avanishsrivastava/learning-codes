package databaseDemo;

import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import static databaseDemo.DBType.MYSQL;
import static databaseDemo.DBType.ORACLE;

public class JavaToDatabaseDemo {

    private Connection connection;
    private ResultSet rs = null;
    private Statement stmt = null;

    @Test
    public void testOracleConnection() {
        try {
            connection = DBUtil.getConnection(ORACLE);
            System.out.println("Connection to QALZ database successfully.");
            connection.close();
            System.out.println("Connection to QALZ database closed.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMYSQLConnection() {
        try {
            connection = DBUtil.getConnection(MYSQL);
            System.out.println("Connection to MYSQL database successfully.");
            connection.close();
            System.out.println("Connection to MYSQL database closed.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDatafromDatabase() {

        try {
            connection = DBUtil.getConnection(ORACLE);
            stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery("select TRANSACTION_CODE, DESCRIPTION, CATEGORY from NETSPEND.TRANSACTION_TYPE where rownum <=10");
//            rs.last();
//            System.out.println("Total rows = " + rs.getRow());
            String format = "%-5d%-25s%-2d";
            while (rs.next()) {
                System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
                System.out.println();
            }

        } catch (SQLException e) {
            DBUtil.showErrorMessage(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                DBUtil.showErrorMessage(e);
            }
        }
    }

    @Test
    public void scrollingResultSet() {
        try (
                Connection connection = DBUtil.getConnection(ORACLE);
                Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt.executeQuery("select TRANSACTION_CODE, DESCRIPTION, CATEGORY from JAVA_TESTING_TABLE where rownum <=10")
        ) {
            String format = "%-5d%-25s%-2d";
            rs.beforeFirst();
            System.out.println("First 10 rows ----------------");
            while (rs.next()) {
                System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
                System.out.println();
            }

            rs.afterLast();
            System.out.println("Last 10 rows ----------------");
            while (rs.previous()) {
                System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
                System.out.println();
            }

            rs.first();
            System.out.println("Record at First row");
            System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
            System.out.println();

            rs.last();
            System.out.println("Record at Last row");
            System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
            System.out.println();

            rs.absolute(4);
            System.out.println("Record at 4th row");
            System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
            System.out.println();

            rs.relative(2);
            System.out.println("Record at 6th row");
            System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
            System.out.println();

            rs.relative(-4);
            System.out.println("Record at 2nd row");
            System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
            System.out.println();

        } catch (SQLException e) {
            DBUtil.showErrorMessage(e);
        }
    }

    @Test
    public void updateResultSet() {
        try (
                Connection connection = DBUtil.getConnection(ORACLE);
                Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                ResultSet rs = stmt.executeQuery("select TRANSACTION_CODE, DESCRIPTION, CATEGORY from JAVA_TESTING_TABLE")
        ) {

            rs.first();
            rs.updateString("DESCRIPTION", "NON-OLTP Creation");
            rs.updateRow();

            System.out.println("Record updated successfully.");

            rs.moveToInsertRow();
            rs.updateInt("TRANSACTION_CODE", 104);
            rs.updateString("DESCRIPTION", "New Testing Transaction ");
            rs.updateInt("CATEGORY", 2);
            rs.insertRow();

            System.out.println("Record inserted successfully");


        } catch (SQLException e) {
            DBUtil.showErrorMessage(e);
        }
    }

    @Test
    public void retreiveDataUsingPreparedStatement() {
        String sql = "select TRANSACTION_CODE, DESCRIPTION, CATEGORY from JAVA_TESTING_TABLE where TRANSACTION_CODE > ? and CATEGORY > ?";
        try (
                Connection connection = DBUtil.getConnection(ORACLE);
                PreparedStatement stmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ) {
            stmt.setInt(1, 100);
            stmt.setShort(2, (short) 10);
            rs = stmt.executeQuery();
            String format = "%-5d%-25s%-2d";
            System.out.println("Total row count = " + rs.getRow());
            while (rs.next()) {
                System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
                System.out.println();
            }
            rs.last();
            System.out.println("Total row count = " + rs.getRow());

        } catch (SQLException e) {
            DBUtil.showErrorMessage(e);
        }
    }

    @Test
    public void insertUsingPreparedStatement() {
        String sql = "insert into JAVA_TESTING_TABLE values (?,?,?,?,?)";
        try (
                Connection connection = DBUtil.getConnection(ORACLE);
                PreparedStatement pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

        ) {
            pstmt.setInt(1, 300);
            pstmt.setString(2, "Testing Transaction");
            pstmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            pstmt.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            pstmt.setByte(5, (byte) 2);
            int result = pstmt.executeUpdate();
            if (result == 1) {
                System.out.println("Inserted successfully.");
            }

        } catch (SQLException e) {
            DBUtil.showErrorMessage(e);
        }
    }

    @Test
    public void updateUsingPreparedStatement() {
        String sql = "update JAVA_TESTING_TABLE set TRANSACTION_CODE = ? where TRANSACTION_CODE = ?";
        try (
                Connection connection = DBUtil.getConnection(ORACLE);
                PreparedStatement pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

        ) {
            pstmt.setInt(1, 999);
            pstmt.setInt(2, 300);
            int result = pstmt.executeUpdate();
            if (result == 1) {
                System.out.println("Record updated successfully.");
            } else
                System.out.println("Error while updating the record");
        } catch (SQLException e) {
            DBUtil.showErrorMessage(e);
        }
    }

    @Test
    public void deleteUsingPreparedStatement() {
        String sql = "delete from JAVA_TESTING_TABLE where TRANSACTION_CODE = ?";
        try (
                Connection connection = DBUtil.getConnection(ORACLE);
                PreparedStatement pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

        ) {
            pstmt.setInt(1, 999);
            int result = pstmt.executeUpdate();
            if (result == 1) {
                System.out.println("Record deleted successfully.");
            } else
                System.out.println("Error while deleting the record");
        } catch (SQLException e) {
            DBUtil.showErrorMessage(e);
        }
    }

    @Test
    public void testTime() {
        Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
        System.out.println(timeStamp);

        System.out.println(timeStamp.getTime());
    }


}
