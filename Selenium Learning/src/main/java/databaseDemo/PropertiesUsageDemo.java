package databaseDemo;

import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class PropertiesUsageDemo {

    @Test
    public void getDataFromPropertiesFile() {
        Properties properties = new Properties();
        InputStream input = null;
        String url = properties.getProperty("oracleURL");
        String userName = properties.getProperty("oracleUser");
        String password = properties.getProperty("oraclePassword");

        try {
            input = new FileInputStream("./resources/config.properties");
            properties.load(input);
            Connection connection = DriverManager.getConnection(url, userName, password);
            Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery("select TRANSACTION_CODE, DESCRIPTION, CATEGORY from NETSPEND.TRANSACTION_TYPE where rownum <=10");

            String format = "%-5d%-25s%-2d";
            while (rs.next()) {
                System.out.format(format, rs.getInt("TRANSACTION_CODE"), rs.getString("DESCRIPTION"), rs.getInt("CATEGORY"));
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            DBUtil.showErrorMessage(e);
        }
    }

    @Test
    public void setDataIntoPropertiesFile() {

        Properties properties = new Properties();
        InputStream input = null;
        OutputStream output = null;

        try {
            input = new FileInputStream("./resources/config.properties");
            properties.load(input);
            properties.setProperty("newURL", "https://www.flipkart.com");
            properties.setProperty("userName", "avanish.1989@gmail.com");
            properties.setProperty("password", "8010865791");
            output = new FileOutputStream("./resources/config.properties");
            properties.store(output, "Saving Flipkart credentials");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
