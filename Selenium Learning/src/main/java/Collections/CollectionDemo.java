package Collections;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class CollectionDemo {
    public static void main (String ar[]) {

        System.out.println("Hello World");
        System.gc();

        List<String> arrlist = new ArrayList<String>();
        arrlist.add("hello");
        arrlist.add("my");
        arrlist.add("name");
        arrlist.add("is");
        arrlist.add("name");
        arrlist.add("kumar");
        arrlist.add("name");

        System.out.println("Printing arraylist :"  +arrlist);

        Set<String> setList = new LinkedHashSet<String>();
        setList.addAll(arrlist);

        setList.add("name");

        arrlist.clear();

        arrlist.addAll(setList);

        System.out.println("Printing arraylist :"  +arrlist);

    }

}

