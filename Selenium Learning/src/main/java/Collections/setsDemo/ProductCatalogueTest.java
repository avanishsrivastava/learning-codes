package Collections.setsDemo;

import org.testng.annotations.Test;

import static Collections.listDemo.ProductFixtures.bobs;
import static Collections.listDemo.ProductFixtures.kates;

public class ProductCatalogueTest {

    @Test
    public void shouldOnlyHoldUniqueProducts() {
        ProductCatalogue catalogue = new ProductCatalogue();
        catalogue.isSuppliedBy(bobs);
        catalogue.isSuppliedBy(kates);
    }

}
