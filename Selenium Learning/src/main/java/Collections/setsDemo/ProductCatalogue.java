package Collections.setsDemo;

import Collections.listDemo.Product;

import java.util.*;

public class ProductCatalogue implements Iterable<Product> {

    //private final Set<Product> products = new HashSet<>();

    private final List<Product> products = new ArrayList<>();

    public void isSuppliedBy(Supplier supplier) {
        products.addAll(supplier.products());
    }

    public Iterator<Product> iterator() {
        return products.iterator();
    }

}
