package Collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayListDemo {

    public static void main(String ar[]) {

        List<String> list = new ArrayList<>();
        list.add("Ravi");
        list.add("kumar");
        list.add("is");
        list.add("from");
        list.add("Delhi");

        System.out.println(list);

        Iterator<String> itr = list.iterator();
        while(itr.hasNext()) {
            System.out.println(itr.next());
        }

    }
}
