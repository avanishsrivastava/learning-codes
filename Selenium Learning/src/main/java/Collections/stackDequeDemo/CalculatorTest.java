package Collections.stackDequeDemo;

import org.testng.annotations.Test;

public class CalculatorTest {

    private Calculator calculator = new Calculator();

    @Test
    public void shouldEvaluateConstants() {
        int result = calculator.evaluate("1");
        System.out.println("Result: " +result);
    }

    @Test
    public void shouldEvaluateAddition() {
        int result = calculator.evaluate("1 + 2");
        System.out.println("Result: " +result);
    }

    @Test
    public void shouldEvaluateSubstraction() {
        int result = calculator.evaluate("3 - 9");
        System.out.println("Result: " +result);
    }

    @Test
    public void shouldEvaluateComplexEquation() {
        int result = calculator.evaluate("5 + 4 - 3 + 9 - 12");
        System.out.println("Result :" +result);
    }
}
