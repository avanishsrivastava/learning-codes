package Collections.stackDequeDemo;

import java.util.ArrayDeque;
import java.util.Deque;

import static java.lang.Integer.parseInt;

public class Calculator {

    public int evaluate(final String input) {
        final Deque<String> stack = new ArrayDeque<>();
        final String[] tokens = input.split(" ");
        for(String token : tokens) {
            stack.add(token);
        }

        while (stack.size() > 1 ) {
            int leftVal = parseInt(stack.pop());
            String operator = stack.pop();
            int rightVal = parseInt(stack.pop());

            int result = 0;

            switch (operator) {
                case "+":
                    result = leftVal + rightVal;
                    break;
                case "-":
                    result = leftVal - rightVal;
                    break;
            }

            stack.push(String.valueOf(result));
        }
        return parseInt(stack.pop());
    }
}
