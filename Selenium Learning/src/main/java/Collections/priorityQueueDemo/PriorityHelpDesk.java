package Collections.priorityQueueDemo;

import Collections.queueDemo.Category;
import Collections.queueDemo.Customer;
import Collections.queueDemo.Enquiry;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import static Collections.queueDemo.Category.*;

public class PriorityHelpDesk {

    private static final Comparator<Enquiry> BY_CATEGORY = new Comparator<Enquiry>() {
        @Override
        public int compare(Enquiry o1, Enquiry o2) {
            return o1.getCategory().compareTo(o2.getCategory());
        }
    };

    private final Queue<Enquiry> enquiries = new PriorityQueue<>(BY_CATEGORY);

    public void enquiry(final Customer customer, final Category category) {
        enquiries.offer(new Enquiry(customer, category));
    }

    public void processAllenquiry() {
//        while(!enquiries.isEmpty()) {
//            final Enquiry enquiry = enquiries.remove();
//            enquiry.getCustomer().reply("Have you tried turning it on or off");
//        }
        Enquiry enquiry;
        while ((enquiry = enquiries.poll()) != null)
            enquiry.getCustomer().reply("Have you tried turning it on or off");

    }

    public static void main(String ar[]) {

        PriorityHelpDesk helpdesk = new PriorityHelpDesk();
        helpdesk.enquiry(Customer.jack, COMPUTER);
        helpdesk.enquiry(Customer.rose, PRINTER);
        helpdesk.enquiry(Customer.sam, PHONE);
        helpdesk.enquiry(Customer.smith, TABLET);

        helpdesk.processAllenquiry();
    }

}
