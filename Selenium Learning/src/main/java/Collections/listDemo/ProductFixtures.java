package Collections.listDemo;


import Collections.setsDemo.Supplier;

public class ProductFixtures {

    public static Product door = new Product("Wooden Door", 35);
    public static Product glassDoor = new Product("Glass Door", 40);
    public static Product floorPanel = new Product("Floor Panel", 25);
    public static Product window = new Product("Window", 20);
    public static Product glassWindow = new Product("Glass Window", 10);
    public static Product woodenWindow = new Product("Wooden Window", 15);


    public static Supplier bobs = new Supplier("Bob's Household Supplies");
    public static Supplier kates = new Supplier("Kate's Home Goods");

    static  {
        bobs.products().add(door);
        bobs.products().add(floorPanel);

        kates.products().add(window);
        kates.products().add(floorPanel);
    }
}
