package Collections.listDemo;

import java.util.ArrayList;
import java.util.List;

public class ListExample {

    public static void main(String ar[]) {

        Product door = new Product("Wooden Door", 35);
        Product floorPanel = new Product("Floor Panel", 25);
        Product window = new Product("Glass Window", 10);

        List<Product> products = new ArrayList<>();
        products.add(door);
        products.add(window);
        products.add(2,floorPanel);
        products.add(door);
        products.add(window);
        products.add(door);
        products.add(window);
        products.add(door);
        products.add(window);

        System.out.println(products.get(0));

        System.out.println(products.get(2));

        products.remove(2);

//        for(Product prod : products) {
//            System.out.println(prod);
//        }

        List<Product> subList = products.subList(4,7);
        for(Product prod : subList) {
            System.out.println(prod);
        }
    }
}
