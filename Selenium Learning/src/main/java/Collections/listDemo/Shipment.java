package Collections.listDemo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Shipment implements Iterable<Product> {

    private static final int LIGHT_VAN_WEIGHT_MAX = 20;
    private static final int PRODUCT_NOT_PRESENT = -1;

    public final List<Product> productList = new ArrayList<>();

    private List<Product> lightVanProducts = new ArrayList<>();
    private List<Product> heavyVanProducts = new ArrayList<>();

    public void add(Product product) {
        productList.add(product);
    }

    public void replace(Product oldProduct, Product newProduct) {
        final int oldProductindex = productList.indexOf(oldProduct);
        if (oldProductindex != PRODUCT_NOT_PRESENT)
            productList.set(oldProductindex, newProduct);

    }

    public void prepare() {
//        for (Product product : productList) {
//            if (product.getWeight() <= LIGHT_VAN_WEIGHT_MAX)
//                lightVanProducts.add(product);
//            else
//                heavyVanProducts.add(product);
//        }

        productList.sort(Product.BY_WEIGHT);
        int splitPoint = findSplitPoint();
        lightVanProducts = productList.subList(0,splitPoint);
        heavyVanProducts = productList.subList(splitPoint,productList.size());
    }

    private int findSplitPoint(){
        int splitPoint=0;
        for(int i=0; i<productList.size(); i++) {
            final Product product = productList.get(i);
            if(product.getWeight() > LIGHT_VAN_WEIGHT_MAX)
                splitPoint = i;
        }
        return splitPoint;
    }

    public List<Product> getLightVanProducts() {
        return lightVanProducts;
    }

    public List<Product> getHeavyVanProducts() {
        return heavyVanProducts;
    }

    public Iterator<Product> iterator() {
        return productList.iterator();
    }


}
