package Collections.listDemo;


import org.testng.annotations.Test;

import static Collections.listDemo.ProductFixtures.*;

public class ShipmentTest  {

    private Shipment shipment = new Shipment();

    @Test
    public void shouldAddItems() {
        shipment.add(door);
        shipment.add(window);
        System.out.println(shipment.productList);
    }

    @Test
    public void shouldReplaceItems() {
        shipment.add(door);
        shipment.add(window);
        System.out.println(shipment.productList);
        shipment.replace(door, floorPanel);
        System.out.println(shipment.productList);
    }

    @Test
    public void shouldNotReplaceMissingItem() {
        shipment.add(door);
        System.out.println(shipment.productList);
        shipment.replace(window,floorPanel);
        System.out.println(shipment.productList);
    }

    @Test
    public void shouldIdentifyVanRequirements() {
        shipment.add(door);
        shipment.add(glassDoor);
        shipment.add(glassWindow);
        shipment.add(woodenWindow);
        shipment.add(window);
        shipment.add(floorPanel);

        shipment.prepare();

        System.out.println(shipment.getLightVanProducts());
        System.out.println(shipment.getHeavyVanProducts());

    }
}
