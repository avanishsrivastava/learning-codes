package Collections;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReadCSV {

    public static void main(String ar[]) {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(".//Business Activity COA Assignment.csv"));
            String[] headers = reader.readLine().split(",");
//            System.out.println(headers.length);
//            for(String header : headers)
//            System.out.println(header);
            String[] values = reader.readLine().split(",");
//            System.out.println(values.length);
//            for(String value :values)
//            System.out.println(value);
            Map<String, String> columnData = new HashMap<>();
            for (int i = 0; i < headers.length; i++) {
                columnData.put(headers[i], values[i]);
            }
            System.out.println(columnData.get("Transaction Code"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
