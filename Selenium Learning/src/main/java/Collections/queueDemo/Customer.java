package Collections.queueDemo;

public class Customer {

    public static final Customer jack = new Customer("Jack");
    public static final Customer sam = new Customer("Sam");
    public static final Customer rose = new Customer("Rose");
    public static final Customer smith = new Customer("Smith");

    private final String name;

    public Customer(final String name) {
        this.name = name;
    }

    public void reply(final String message) {
        System.out.printf("%s: %s \n", name, message);
    }
}
