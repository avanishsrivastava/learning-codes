package Collections.queueDemo;

import java.util.ArrayDeque;
import java.util.Queue;

public class HelpDesk {

    private final Queue<Enquiry> enquiries = new ArrayDeque<>();

    public void enquiry(final Customer customer, final Category category) {
        enquiries.offer(new Enquiry(customer, category));
    }

    public void processAllenquiry() {
//        while(!enquiries.isEmpty()) {
//            final Enquiry enquiry = enquiries.remove();
//            enquiry.getCustomer().reply("Have you tried turning it on or off");
//        }
        Enquiry enquiry;
        while ((enquiry = enquiries.poll()) != null)
            enquiry.getCustomer().reply("Have you tried turning it on or off");

    }

    public static void main(String ar[]) {

        HelpDesk helpdesk = new HelpDesk();
        helpdesk.enquiry(Customer.jack, Category.COMPUTER);
        helpdesk.enquiry(Customer.rose, Category.PRINTER);
        helpdesk.enquiry(Customer.smith, Category.TABLET);
        helpdesk.enquiry(Customer.sam, Category.PHONE);


        helpdesk.processAllenquiry();
    }

}
