package Collections.queueDemo;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.function.Predicate;

import static Collections.queueDemo.Category.*;

public class CategorisedHelpdesk {

    private final Queue<Enquiry> enquiries = new ArrayDeque<>();

    public void enquiry(final Customer customer, final Category category) {
        enquiries.offer(new Enquiry(customer, category));
    }

    public void processPrinterEnquiry() {
        processEnquiry(enq -> enq.getCategory() == PRINTER, "Is it out of paper");
    }

    public void processGeneralEnquiry() {
        processEnquiry(enq -> enq.getCategory() != PRINTER, "Have you tried turning it on or off");
    }

    private void processEnquiry(Predicate<Enquiry> predicate, String message) {
        final Enquiry enquiry = enquiries.peek();
        if (enquiry.getCustomer() != null && predicate.test(enquiry)) {
            enquiries.remove();
            enquiry.getCustomer().reply(message);
        }
        else
            System.out.println("No work to do");
    }

    public static void main(String ar[]) {

        CategorisedHelpdesk helpdesk = new CategorisedHelpdesk();
        helpdesk.enquiry(Customer.jack, COMPUTER);
        helpdesk.enquiry(Customer.rose, PRINTER);
        helpdesk.enquiry(Customer.sam, PHONE);
        helpdesk.enquiry(Customer.smith, TABLET);

        helpdesk.processPrinterEnquiry();
        helpdesk.processGeneralEnquiry();
        helpdesk.processPrinterEnquiry();
        helpdesk.processGeneralEnquiry();
        helpdesk.processPrinterEnquiry();
        helpdesk.processGeneralEnquiry();
    }


}
