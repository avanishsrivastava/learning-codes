package testNGDemo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class FirstTestNGClass {

    private WebDriver driver;

    @BeforeSuite
    public void initiateBrowser() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-arguments");
        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        driver = new ChromeDriver(options);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void firstTest() {
        driver.get("https://www.google.com");
        System.out.println("Page Title " + driver.getTitle());
    }

    @AfterSuite
    public void closeBrowser() {
        driver.quit();
    }


}
