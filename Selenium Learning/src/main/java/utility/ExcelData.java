package utility;

public class ExcelData {
    private String title;
    private String author;
    private String edition;
    private double price;

    public ExcelData(String title, String author, double price, String edition) {
        this.title = title;
        this.author = author;
        this.price = price;
        this.edition = edition;

    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
