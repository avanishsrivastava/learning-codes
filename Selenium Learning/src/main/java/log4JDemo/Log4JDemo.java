package log4JDemo;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Log4JDemo {

    Logger log = Logger.getLogger(Log4JDemo.class.getName());

    @Test
    public void testLogging() {
        PropertyConfigurator.configure("./resources/log4J.properties");
        log.debug("This is debug message");
        log.info("This is info message");
    }

    @Test
    public void testLoggingMethods() {
        PropertyConfigurator.configure("./resources/log4J.properties");
        log.setLevel(Level.WARN);
        log.debug("This is debug message");
        log.error("This is error message");
        log.fatal("This is fatal message");
        log.info("this is info message");
        log.warn("This is warn message");
        log.trace("This is trace message");
    }

    @Test
    public void testDevpinoyLogger() {
        PropertyConfigurator.configure("./resources/log4JMain.properties");
        log = Logger.getLogger("devpinoyLogger");
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        options.setExperimentalOption("prefs", prefs);
        System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
        WebDriver driver = new ChromeDriver(options);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        log.info("Launched website");
    }
}
