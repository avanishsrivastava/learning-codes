package javaacriptExecutorDemo;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import utility.BaseClass;

public class JSExecutorDemo extends BaseClass {

    JavascriptExecutor js = (JavascriptExecutor)driver;

    @Test
    public void testExecuteAsyncScript() {
        driver.get("http://demo.guru99.com/V4/index.php");
        long startTime = System.currentTimeMillis();
        js.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 5000);");
        System.out.println("Passes time: " +(System.currentTimeMillis() - startTime));

    }

    @Test
    public void scrollBy() throws InterruptedException {
        driver.get("https://en.wikipedia.org/wiki/Main_Page");
        js.executeScript("window.scrollBy(0,600);");
        Thread.sleep(5000);
    }

    @Test
    public void scrollIntoView() {
        driver.get("https://en.wikipedia.org/wiki/Main_Page");
        js.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.cssSelector("#metalink > strong > a")));
        driver.findElement(By.cssSelector("#metalink > strong > a")).click();
    }


}
