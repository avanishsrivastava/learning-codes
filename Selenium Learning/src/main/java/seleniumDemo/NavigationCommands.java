package seleniumDemo;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import utility.BaseClass;

public class NavigationCommands extends BaseClass {

    public static void main(String ar[]) {
        driver.get("https://www.google.com");

        driver.findElement(By.id("lst-ib")).sendKeys("selenium hq");

        driver.findElement(By.id("lst-ib")).sendKeys(Keys.ENTER);

        driver.navigate().refresh();

        driver.navigate().back();

        driver.navigate().forward();

        driver.navigate().to("https://www.toolsqz.com");

        driver.quit();
    }
}
