package seleniumDemo;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import utility.BaseClass;


public class DropDownMultiDropDemo extends BaseClass {

    public static void main(String ar[]) throws InterruptedException {
        driver.navigate().to("https://semantic-ui.com/modules/dropdown.html");

        //Click on the dropdown menu and select
//        driver.findElement(By.cssSelector(".ui.active.tab > div:nth-of-type(3) > div:nth-of-type(2)")).click();
//        driver.findElement(By.cssSelector(".ui.selection.dropdown > div[class='menu transition visible'] > div:nth-of-type(1)")).click();
//
//        if ((driver.findElement(By.cssSelector(".ui.active.tab > div:nth-of-type(3) > div:nth-of-type(2) > div:nth-of-type(1)")).getText()).equals("Male"))
//            System.out.println("Male selected successfully");

        //Search and then Select item from a list of drop down
        Actions action = new Actions(driver);
//        action.moveToElement(driver.findElement(By.cssSelector(".ui.active.tab > div:nth-of-type(6) > div:nth-of-type(1) > div:nth-of-type(1)")));
//        action.click();
//        action.sendKeys("ind").perform();
//        action.sendKeys(Keys.ENTER).perform();
//
//        System.out.println((driver.findElement(By.cssSelector(".ui.active.tab > div:nth-of-type(6) > div:nth-of-type(1) > div:nth-of-type(1)")).getText()));

        //Click and multiple selection from list of items
        action.moveToElement(driver.findElement(By.cssSelector(".ui.active.tab > div:nth-of-type(9) > div:nth-of-type(1) > div:nth-of-type(1)")));
        action.click();
        action.sendKeys("ind").sendKeys(Keys.ENTER).perform();
//        action.sendKeys(Keys.ENTER).build().perform();
        Thread.sleep(2000);
//        action.sendKeys("uni").perform();
//        action.sendKeys(Keys.ENTER).build().perform();
//        Thread.sleep(2000);
//        action.sendKeys("sou").build().perform();
//        action.sendKeys(Keys.ENTER).perform();
//        Thread.sleep(5000);

        driver.quit();

    }

}
