package seleniumDemo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utility.BaseClass;

import java.util.List;

public class CheckboxRadioButtonDemo extends BaseClass {
    public static void main(String ar[]) {
        driver.get("http://www.tizag.com/htmlT/htmlcheckboxes.php");
        List<WebElement> checkboxs = driver.findElements(By.xpath("//*[@class='display'][1]/input"));
        for (WebElement checkbox : checkboxs) {
            if (!checkbox.isSelected())
                checkbox.click();
        }

        checkboxs = driver.findElements(By.xpath("//*[@class='display'][2]/input"));
        for (WebElement checkbox : checkboxs) {
            if (!checkbox.isSelected())
                checkbox.click();
        }

        driver.navigate().to("http://www.tizag.com/htmlT/htmlradio.php");
        List<WebElement> radiobuttons = driver.findElements(By.xpath("//*[@class='display'][1]/input"));
        for (WebElement radio : radiobuttons) {
            if (radio.isDisplayed())
                radio.click();
        }

    }
}
