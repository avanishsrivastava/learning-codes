package seleniumDemo;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import utility.BaseClass;


public class BrowserCommands extends BaseClass {
    public static void main(String ar[]) {
        driver.get("https://www-netspend-c.test.aus.netspend.net/prepaid-debit-card/applyNow.m?ctxName=online_resp_NC_visa");

        driver.findElement(By.id("firstNameElem")).sendKeys("Tester");

        driver.findElement(By.id("lastNameElem")).sendKeys("TesterNew");

        driver.findElement(By.id("fieldStreetAddress")).sendKeys("701 Brazos");

        driver.findElement(By.id("city")).sendKeys("Austin");

        Select stateName = new Select(driver.findElement(By.id("state")));

        stateName.selectByVisibleText("TX");

        driver.findElement(By.id("fieldZipCode")).sendKeys("78701");

        driver.findElement(By.id("fieldEmailAddress")).sendKeys("testingnewtest@test.com");

        Select eDDE = new Select(driver.findElement(By.name("employerDirectDepositEligibility")));

        eDDE.selectByIndex(4);

        driver.findElement(By.id("termsRead")).click();

        driver.findElement(By.name("finish")).click();

        System.out.println("Executed successfully");

        driver.quit();
    }

}
