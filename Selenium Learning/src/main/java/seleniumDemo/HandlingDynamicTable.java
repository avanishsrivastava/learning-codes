package seleniumDemo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utility.BaseClass;

import java.util.List;

public class HandlingDynamicTable extends BaseClass {

    public static void main(String ar[]) {
        driver.navigate().to("http://www.espncricinfo.com/series/18544/scorecard/1133983/india-vs-afghanistan-only-test-afghanistan-tour-of-india-2018");
        WebElement board = driver.findElement(By.cssSelector(".col-b > div:nth-of-type(3) > article:nth-of-type(3) >ul >li >div:nth-of-type(2) > div:nth-of-type(1)"));
        List<WebElement> rows = board.findElements(By.tagName("div"));
        for(WebElement row: rows) {
            System.out.print(row.findElement(By.cssSelector("/")) .getText() + "\t");
            System.out.println();
        }


    }

}
