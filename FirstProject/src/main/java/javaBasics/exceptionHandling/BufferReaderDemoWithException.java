package javaBasics.exceptionHandling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferReaderDemoWithException {

    public static void main(String ar[]) {

        BufferedReader reader = null;
        int total = 0;
        try {
            reader = new BufferedReader(new FileReader("D:\\Numbers.txt"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                total += Integer.parseInt(line);
            }
            System.out.println("Total : " + total);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if(reader != null)
                    reader.close();
            } catch(IOException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }

    }

}
