package javaBasics.CalcDemoUsingInheritance;

public class Divide extends CalculateBase {

    public Divide() {

    }

    public Divide(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        if(!(getRightVal()==0)) {
            double resultVal = getLeftVal() / getRightVal();
            setResult(resultVal);
        }
        else
            System.out.println("Cannot Divide by zero");

    }
}
