package javaBasics.CalcDemoUsingInheritance;

public class Subtractor extends CalculateBase {

    public Subtractor() {

    }

    public Subtractor(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        double resultVal = getLeftVal() - getRightVal();
        setResult(resultVal);
    }

}
