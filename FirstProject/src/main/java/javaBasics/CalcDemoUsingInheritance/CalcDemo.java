package javaBasics.CalcDemoUsingInheritance;

public class CalcDemo {

    public static void main(String ar[]) {

        System.out.println("User Inheritance");

        CalculateBase[] calculateBases = {
                new Adder(5.0d, 7.0d),
                new Subtractor(9.76d, 8.12d),
                new Multiplier(2.7d,9.671d),
                new Divide(3.12d,0.0d)
        };

        for(CalculateBase base : calculateBases) {
            base.calculate();
            System.out.println(base.getResult());
        }

    }

}
