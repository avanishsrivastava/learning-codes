package javaBasics;

public enum FlightCrewJob {

    Pilot,
    CoPilot,
    FlightAttendent,
    AirMarshal

}
