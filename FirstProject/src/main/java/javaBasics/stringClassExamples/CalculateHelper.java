package javaBasics.stringClassExamples;

import javaBasics.CalcDemoUsingInheritance.*;

public class CalculateHelper {
    private static final char ADD_SYMBOL = '+';
    private static final char SUBTRACT_SYMBOL = '-';
    private static final char MULTIPLY_SYMBOL = '*';
    private static final char DIVIDE_SYMBOL = '/';
    MathCommand command;
    private double leftVal;
    private double rightVal;

    public double getLeftVal() {
        return leftVal;
    }

    public void setLeftVal(double leftVal) {
        this.leftVal = leftVal;
    }

    public double getRightVal() {
        return rightVal;
    }

    public void setRightVal(double rightVal) {
        this.rightVal = rightVal;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    private double result;


    public void process(String statement) {
        String[] parts = statement.split(" ");
        String commandString = parts[0];
        leftVal = Double.parseDouble(parts[1]);
        rightVal = Double.parseDouble(parts[2]);

        setCommandFromString(commandString);

        CalculateBase calculateBase = null;

        switch (command) {
            case Add:
                calculateBase = new Adder(leftVal, rightVal);
                break;
            case Subtract:
                calculateBase = new Subtractor(leftVal, rightVal);
                break;
            case Multiply:
                calculateBase = new Multiplier(leftVal, rightVal);
                break;
            case Divide:
                calculateBase = new Divide(leftVal, rightVal);
                break;
        }

        calculateBase.calculate();
        result = calculateBase.getResult();
    }

    private void setCommandFromString(String commandString) {
        if (commandString.equalsIgnoreCase(MathCommand.Add.toString()))
            command = MathCommand.Add;
        else if (commandString.equalsIgnoreCase(MathCommand.Subtract.toString()))
            command = MathCommand.Subtract;
        else if (commandString.equalsIgnoreCase(MathCommand.Multiply.toString()))
            command = MathCommand.Multiply;
        else if (commandString.equalsIgnoreCase(MathCommand.Divide.toString()))
            command = MathCommand.Divide;
    }

    @Override
    public String toString() {
        char symbol = ' ';
        switch (command) {
            case Add:
                symbol = ADD_SYMBOL;
                break;
            case Subtract:
                symbol = SUBTRACT_SYMBOL;
                break;
            case Multiply:
                symbol = MULTIPLY_SYMBOL;
                break;
            case Divide:
                symbol = DIVIDE_SYMBOL;
                break;
        }

        StringBuilder sb = new StringBuilder(40);
        sb.append(leftVal);
        sb.append(" " + symbol+ " ");
        sb.append(rightVal);
        sb.append(" = " + result);


        return sb.toString();
    }
}
