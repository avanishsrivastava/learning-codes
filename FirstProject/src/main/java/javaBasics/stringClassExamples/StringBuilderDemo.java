package javaBasics.stringClassExamples;

import javaBasics.inheritanceExample.Flight;

public class StringBuilderDemo {

    public static void main(String ar[]) {

        StringBuilder sb = new StringBuilder(10);
        System.out.println("String Builder capacity is: " + sb.capacity());
        String location = "Florida";
        sb.append("I will fly to ");
        sb.append(location);
        sb.append(" on ");
        Flight myFlight = new Flight(175);
        sb.append(myFlight);

        System.out.println(sb.toString());

        int time = 9;

        int pos = sb.length() - " on ".length() - myFlight.toString().length();

        sb.insert(pos, " at ");
        sb.insert(pos + 4, time);

        System.out.println(sb.toString());

    }

}
