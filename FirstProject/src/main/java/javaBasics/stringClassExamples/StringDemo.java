package javaBasics.stringClassExamples;

import javaBasics.inheritanceExample.Flight;

public class StringDemo {

    public static void main(String ar[]) {

        String s1 = "I love ";
        s1 += "Java.";

        System.out.println(s1);

        String s2 = "I ";
        s2 += "love Java.";

        System.out.println(s2);

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));

        String s3 = s1.intern();
        String s4 = s2.intern();

        System.out.println(s3 == s4);
        System.out.println(s3.equals(s4));

        Flight myFlight = new Flight(175);
        System.out.println(myFlight);

    }

}

