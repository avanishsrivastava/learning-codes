package javaBasics.stringClassExamples;

public class CalcDemoUsingEnum {

    public static void main(String ar[]) {

        String[] statements = {
                "divide 100.00 50.00",
                "add 95.27 54.65",
                "multiply 2.30 4.51",
                "subtract 9.25 8.25"
        };

        CalculateHelper helper = new CalculateHelper();

        for(String statement : statements) {
            helper.process(statement);
//            System.out.println(helper.getResult());
            System.out.println(helper);
        }

    }

}
