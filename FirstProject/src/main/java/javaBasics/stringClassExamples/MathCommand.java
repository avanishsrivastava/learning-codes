package javaBasics.stringClassExamples;

public enum MathCommand {
    Add,
    Subtract,
    Multiply,
    Divide
}
