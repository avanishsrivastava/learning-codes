package javaBasics.inheritanceExample;

public class InheritanceConstructorDemo {

    public static void main(String ar[]) {

        Flight flight = new Flight(175);

        System.out.println(flight.getFlightNumber());

        CargoFlight cf = new CargoFlight(294, 124.5f);

        System.out.println(cf.getFlightNumber());


    }

}
