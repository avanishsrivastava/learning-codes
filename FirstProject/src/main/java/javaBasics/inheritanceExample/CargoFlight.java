package javaBasics.inheritanceExample;

public class CargoFlight extends Flight {

    float maxCargoSpace = 1000.0f;
    float usedCargoSpace;
    int seats = 20;

    public CargoFlight(int flightNumber) {
        super(flightNumber);
    }

    public CargoFlight(int flightNumber, float usedCargoSpace) {
        super(flightNumber);
        this.usedCargoSpace = usedCargoSpace;
    }

    public void addPackage(float h, float w, float d) {
        double size = h * w * d;
        if (hasCargoSpace(size))
            usedCargoSpace += size;
        else
            handleNoSpace();

    }

    private boolean hasCargoSpace(double size) {
        return usedCargoSpace + size <= maxCargoSpace;
    }

    private void handleNoSpace() {
        System.out.println("Not enough space");

    }

    @Override
    public int getSeats() {
        return  seats;
    }

}
