package javaBasics.inheritanceExample;

public class Flight {

    int passengers, flightNumber, seats = 150;
    private char flightClass = 'A';
    private boolean[] isSeatAvailable;
//
//    public FlightWithInitialBlock() {
//        isSeatAvailable = new boolean[seats];
//        for(int i = 0; i< seats; i++)
//            isSeatAvailable[i] = true;
//    }

    {
        isSeatAvailable = new boolean[seats];
        for (int i = 0; i < seats; i++)
            isSeatAvailable[i] = true;
    }

    public Flight() {
    }

    public Flight(int flightNumber) {
        //this();
        this.flightNumber = flightNumber;
    }

    public Flight(char flightClass) {
        //this();
        this.flightClass = flightClass;
    }

    public Flight(int flightNumber, char flightClass) {
        //this();
        this.flightClass = flightClass;
        this.flightNumber = flightNumber;
    }

    public void addPassenger() {
//        for (boolean seat : isSeatAvailable) {
//            System.out.println(seat);
//        }
        System.out.println("defaultPack.Flight Class : " + flightClass);
        System.out.println("defaultPack.Flight Number : " + flightNumber);
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public int getSeats() {
        return seats;
    }

    @Override
    public boolean equals(Object o) {

        if (!(o instanceof Flight))
            return false;
        Flight other = (Flight) o;

        return
                flightNumber == other.flightNumber && flightClass == other.flightClass;
    }

    @Override
    public String toString() {
        return "Flight #" + flightNumber;
    }
}


