package javaBasics.inheritanceExample;

public class FieldHidingMethodOverridingDemo {

    public static void main(String ar[]) {

        Flight flight = new Flight();
        System.out.println("Seats in flight class");
        System.out.println(flight.seats);
        System.out.println(flight.getSeats());

        CargoFlight cf = new CargoFlight(175);
        System.out.println("Seats in cargo flight class");
        System.out.println(cf.seats);
        System.out.println(cf.getSeats());

        //Reference is of flight class so showing the seat value of flight class
        Flight newFlight = new CargoFlight(175);
        System.out.println("Seats in flight reference of cargo class");
        System.out.println(newFlight.seats);
        System.out.println(newFlight.getSeats());


    }
}
