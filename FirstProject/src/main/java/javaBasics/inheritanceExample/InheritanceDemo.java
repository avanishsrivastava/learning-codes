package javaBasics.inheritanceExample;

import javaBasics.Constructor.Passenger;

public class InheritanceDemo {

    public static void main(String ar[]) {

        Flight flight = new CargoFlight(175);
        flight.addPassenger();

        if (flight instanceof CargoFlight) {
            CargoFlight cf = (CargoFlight) flight;
            cf.addPackage(1.0f, 2.6f, 3.64f);
        }

        Passenger jane = new Passenger(0, 2);
        flight.addPassenger();

        Object obj = new Object();
        System.out.println(obj.getClass());
        System.out.println(obj.hashCode());


    }

}
