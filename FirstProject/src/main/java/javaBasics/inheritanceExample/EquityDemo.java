package javaBasics.inheritanceExample;

public class EquityDemo {

    public static void main(String ar[]) {

        Flight f1 = new Flight(175);
        Flight f2 = new Flight(175);

//        if(f1==f2) {
//            System.out.println("Same flight");
//        }
//        else
//            System.out.println("Different flight");

        if(f1.equals(f2)) {
            System.out.println("Same flight");
        }
        else
            System.out.println("Different flight");

    }

}
