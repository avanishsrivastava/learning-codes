package javaBasics.staticDemo;

public class Flight {

    private int passengers;
    private int seats = 10;

    static int allPassengers;

    static int getAllPassengers() {
        return allPassengers;
    }

    static void resetAllPassengers() {
        allPassengers = 0;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "passengers=" + passengers +
                ", seats=" + seats +
                '}';
    }

    public void add1Passenger() {
        if (hasSeating()) {
            passengers += 1;
            allPassengers += 1;
        }
        else
            handleTooMany();
    }

    private void handleTooMany() {
        System.out.println("No more seats available");
    }

    private boolean hasSeating() {
        return allPassengers < seats;

    }

}
