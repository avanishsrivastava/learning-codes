package javaBasics.staticDemo;

import static javaBasics.staticDemo.Flight.getAllPassengers;
import static javaBasics.staticDemo.Flight.resetAllPassengers;

public class FlightStaticMembersDemo {

    public static void main(String ar[]) {

        resetAllPassengers();
        System.out.println(getAllPassengers());

        Flight lax015 = new Flight();

        for (int i = 1; i <= 11; i++) {
            lax015.add1Passenger();
            System.out.println(lax015);
        }
    }


}
