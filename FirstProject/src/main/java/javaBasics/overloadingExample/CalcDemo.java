package javaBasics.overloadingExample;

public class CalcDemo {

    public static void main(String ar[]) {

        double leftValue = 9.0d;
        double rightValue = 4.0d;
        int leftVal = 9;
        int rightVal = 4;

        MathEquation equation = new MathEquation('d');
        equation.execute(leftValue, rightValue);

        System.out.println("Result = " + equation.getResult());

        equation.execute(leftVal, rightVal);

        System.out.println("Result = " + equation.getResult());

    }

}
