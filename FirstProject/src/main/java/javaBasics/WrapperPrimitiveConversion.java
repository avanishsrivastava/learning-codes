package javaBasics;

public class WrapperPrimitiveConversion {

    public static void main(String ar[]) {

        Integer a = 100;
        int b = a + 10;
        Integer c = b - 50;

        System.out.printf("%d, %d, %d \n", a, b, c);

        //Boxing
        Integer d = Integer.valueOf(b);
        int e = d.intValue() - 20;

        System.out.printf("%d, %d", d, e);

    }

}
