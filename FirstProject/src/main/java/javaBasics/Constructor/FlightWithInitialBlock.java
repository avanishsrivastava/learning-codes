package javaBasics.Constructor;

public class FlightWithInitialBlock {

    private int passengers, flightNumber, seats = 10;
    private char flightClass;
    private boolean[] isSeatAvailable;
//
//    public FlightWithInitialBlock() {
//        isSeatAvailable = new boolean[seats];
//        for(int i = 0; i< seats; i++)
//            isSeatAvailable[i] = true;
//    }

    {
        isSeatAvailable = new boolean[seats];
        for (int i = 0; i < seats; i++)
            isSeatAvailable[i] = true;
    }

    public FlightWithInitialBlock() {
    }

    ;

    public FlightWithInitialBlock(int flightNumber) {
        //this();
        this.flightNumber = flightNumber;
    }

    public FlightWithInitialBlock(char flightClass) {
        //this();
        this.flightClass = flightClass;
    }
    public FlightWithInitialBlock(int flightNumber, char flightClass) {
        //this();
        this.flightClass = flightClass;
        this.flightNumber = flightNumber;
    }

    public void addPassenger() {
        for(boolean seat : isSeatAvailable) {
            System.out.println(seat);
        }
        System.out.println("defaultPack.Flight Class : " + flightClass);
        System.out.println("defaultPack.Flight Number : " + flightNumber);
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }


}
