package javaBasics.Constructor;

public class Passenger {

    private int freeBags = 1;
    private int checkedBags = 5;
    private double perBagFee = 20.0d;

    public Passenger() {

    }

    public Passenger(int freeBags) {
        this(freeBags > 1 ? 25.0d : 50.0d);
        //perBagFee = freeBags > 1 ? 25.0d : 50.0d;
        this.freeBags = freeBags;
    }

    public Passenger(int freeBags, int checkedBags) {
        this(freeBags);
        perBagFee = freeBags > 1 ? 25.0d : 50.0d;
        this.checkedBags = checkedBags;
    }

    private Passenger(double perBagFee) {
        this.perBagFee = perBagFee;
    }

    public void showInfo() {
        System.out.println("No of Free Bags : " + freeBags);
        System.out.println("No of Checked In Bags : " + checkedBags);
        System.out.println("Total fee applicable : " + (checkedBags - freeBags)* perBagFee);
    }


}
