package Practice.Constructor;

public class PrivateConstructor {

    static PrivateConstructor instance = null;

    private PrivateConstructor() {

    }

    public static PrivateConstructor getInstance() {
        if (instance == null)
                instance = new PrivateConstructor();
        return instance;
    }

    public void showInfo() {
        System.out.println("Example of private constructor");
    }

}
