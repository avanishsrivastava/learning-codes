package javaBasics.Constructor;

public class MathEquationWithConstructor {
    
    private double leftVal;
    private double rightVal;
    private char opCode;
    private double result;
    
    public MathEquationWithConstructor(char opCode) {
        this.opCode = opCode;
    }
    
    public MathEquationWithConstructor(double leftVal, double rightVal, char opCode) {
        this(opCode);
        this.leftVal = leftVal;
        this.rightVal = rightVal;
    }

    public double getResult() {
        return result;
    }

    public void execute() {
        switch (opCode) {
            case 'a':
                result = leftVal + rightVal;
                break;
            case 's':
                result = leftVal - rightVal;
                break;
            case 'm':
                result = leftVal * rightVal;
                break;
            case 'd':
                result = rightVal != 0.0d ? (leftVal / rightVal) : 0.0d;
//                result = leftVal / rightVal;
                break;
            default:
                System.out.println("Error : Invalid opcode");
                break;
        }
    }
    
    
}
