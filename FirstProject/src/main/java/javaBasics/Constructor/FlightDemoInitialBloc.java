package javaBasics.Constructor;

public class FlightDemoInitialBloc {

    public static void main(String ar[]) {

        FlightWithInitialBlock obj1 = new FlightWithInitialBlock();
        obj1.addPassenger();

        obj1 = new FlightWithInitialBlock(125);
        obj1.addPassenger();

        obj1 = new FlightWithInitialBlock('A');
        obj1.addPassenger();

        obj1 = new FlightWithInitialBlock(1356,'A');
        obj1.addPassenger();

    }
}
