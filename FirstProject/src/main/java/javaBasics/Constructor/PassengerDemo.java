package javaBasics.Constructor;

public class PassengerDemo {

    public static void main(String ar[]) {

        Passenger passenger = new Passenger();
        passenger.showInfo();

        Passenger psg1 = new Passenger(2);
        psg1.showInfo();

        Passenger psg2 = new Passenger(2, 8);
        psg2.showInfo();


    }

}
