package javaBasics.Constructor;

public class CalcDemowithConstructor {

    public static void main(String ar[]) {

        MathEquationWithConstructor[] equations = new MathEquationWithConstructor[5];
        equations[0] = new MathEquationWithConstructor(100.0d, 50.0d, 'a');
        equations[1] = new MathEquationWithConstructor(75.0d, 15.0d, 's');
        equations[2] = new MathEquationWithConstructor(15.0d, 5.0d, 'm');
        equations[3] = new MathEquationWithConstructor(100.0d, 25.0d, 'd');
        equations[4] = new MathEquationWithConstructor(100.0d, 0.0d, 'd');

        for(MathEquationWithConstructor equation : equations) {
            equation.execute();
            System.out.println("Result : " +equation.getResult());
        }
    }



}
