package javaBasics.nestedTypesDemo;

public class Passenger implements Comparable {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public static class RewardProgram {
        private int memberLevel;
        private int memberDays;

        public int getMemberLevel() {
            return memberLevel;
        }

        public void setMemberLevel(int memberLevel) {
            this.memberLevel = memberLevel;
        }

        public int getMemberDays() {
            return memberDays;
        }

        public void setMemberDays(int memberDays) {
            this.memberDays = memberDays;
        }

        @Override
        public String toString() {
            return "RewardProgram{" +
                    "memberLevel=" + memberLevel +
                    ", memberDays=" + memberDays +
                    '}';
        }
    }

    private RewardProgram rewardProgram = new RewardProgram();

    public RewardProgram getRewardProgram() {
        return rewardProgram;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "name='" + name + '\'' +
                ", rewardProgram=" + rewardProgram +
                '}';
    }
}
