package javaBasics.nestedTypesDemo;

public class PassengerNestedTypeDemo {

    public static void main(String ar[]) {

        Passenger steve = new Passenger();
        steve.setName("Steve");
        steve.getRewardProgram().setMemberLevel(3);
        steve.getRewardProgram().setMemberDays(180);

        System.out.println(steve);

        Passenger.RewardProgram platinum = new Passenger.RewardProgram();
        platinum.setMemberLevel(3);
        platinum.setMemberDays(180);

        System.out.println(platinum);

        if(steve.getRewardProgram().getMemberLevel() == platinum.getMemberLevel())
            System.out.println("Steve is platinum");
    }

}
