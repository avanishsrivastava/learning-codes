package javaBasics;

import java.util.Scanner;

public class InputFromUser {
    public static void main(String ar[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the first number :");
        int num1 = sc.nextInt();
        System.out.println("Enter the second number :");
        int num2 = sc.nextInt();
        System.out.println("Product of numbers :" + num1 * num2);
    }
}
