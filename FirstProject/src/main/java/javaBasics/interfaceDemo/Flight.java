package javaBasics.interfaceDemo;

import java.util.Iterator;

public class Flight implements Comparable, Iterable<Person>{

    private int flightTime;
    private CrewMember[] crewMembers;
    private Passenger[] passengers;

    public void setFlightTime(int flightTime) {
        this.flightTime = flightTime;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightTime=" + flightTime +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Flight flight = (Flight) o;
        if(flightTime > flight.flightTime)
            return 1;
        else if(flightTime < flight.flightTime)
            return -1;
        else
            return 0;
    }

    @Override
    public Iterator<Person> iterator() {
        return new FlighIterator(crewMembers, passengers);
    }
}
