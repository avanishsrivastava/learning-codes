package javaBasics.interfaceDemo;

public class Passenger extends Person implements Comparable {

    private int memberDays;
    private int memberLevel;

    @Override
    public int compareTo(Object o) {
        Passenger psg = (Passenger) o;
        if (memberLevel > psg.memberLevel)
            return -1;
        else if (memberLevel < psg.memberLevel)
            return 1;
        else {
            if (memberDays > psg.memberDays)
                return -1;
            else if (memberDays < psg.memberDays)
                return 1;
            else
                return 0;
        }
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "memberDays=" + memberDays +
                ", memberLevel=" + memberLevel +
                '}';
    }

    public void setLevelAndDays(int memberDays, int memberLevel) {
        this.memberDays = memberDays;
        this.memberLevel = memberLevel;
    }
}
