package javaBasics.interfaceDemo;

import java.util.Arrays;

public class FlightInterfaceDemo {

    public static void main(String ar[]) {

        Flight sl045 = new Flight();
        sl045.setFlightTime(45);

        Flight lx015 = new Flight();
        lx015.setFlightTime(15);

        Flight us90 = new Flight();
        us90.setFlightTime(90);

        Flight fx30 = new Flight();
        fx30.setFlightTime(30);

        Flight[] flights = {sl045, lx015, us90, fx30};
        System.out.println();
        System.out.println("Flights before sorting");

        for(Flight flight : flights) {
            System.out.println(flight);
        }

        Arrays.sort(flights);

        System.out.println();
        System.out.println("Flights after sorting");

        for(Flight flight : flights) {
            System.out.println(flight);
        }



    }

}
