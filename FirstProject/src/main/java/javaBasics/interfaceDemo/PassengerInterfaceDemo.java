package javaBasics.interfaceDemo;

import java.util.Arrays;

public class PassengerInterfaceDemo {

    public static void main(String ar[]) {
        Passenger bob = new Passenger();
        bob.setLevelAndDays(1, 180);

        Passenger jane = new Passenger();
        jane.setLevelAndDays(1, 90);

        Passenger james = new Passenger();
        james.setLevelAndDays(2, 340);

        Passenger kate = new Passenger();
        kate.setLevelAndDays(3, 749);

        Passenger[] passengers = {kate, bob, james, jane};
        for(Passenger psg : passengers) {
            System.out.println(psg);
        }

        Arrays.sort(passengers);

        for(Passenger psg : passengers) {
            System.out.println(psg);
        }

    }
}
