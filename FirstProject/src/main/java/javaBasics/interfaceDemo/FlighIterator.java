package javaBasics.interfaceDemo;

import java.util.Iterator;

public class FlighIterator implements Iterator<Person> {
    private CrewMember[] crew;
    private Passenger[] roster;
    private int index = 0;

    public FlighIterator(CrewMember[] crew, Passenger[] roster) {
        this.crew = crew;
        this.roster = roster;
    }

    @Override
    public boolean hasNext() {
        return index < (crew.length + roster.length);
    }

    @Override
    public Person next() {
        Person p = (index < crew.length) ?
                crew[index] : roster[index - crew.length];
        index++;
        return p;
    }
}
