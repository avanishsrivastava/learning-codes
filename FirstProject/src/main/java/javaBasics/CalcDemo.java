package javaBasics;

public class CalcDemo {
    public static void main(String ar[]) {

        MathEquation[] equations = new MathEquation[5];

        equations[0] = createData(100.0d, 50.0d, 'a');
        equations[1] = createData(75.0d, 15.0d, 's');
        equations[2] = createData(15.0d, 5.0d, 'm');
        equations[3] = createData(100.0d, 25.0d, 'd');
        equations[4] = createData(100.0d, 0.0d, 'd');

        for( MathEquation equation : equations) {
            equation.execute();
            System.out.println("Result: " +equation.getResult());
        }


    }

    public static MathEquation createData(double leftVal, double rightVal, char opsCode) {

        MathEquation equation = new MathEquation();
        equation.setLeftValue(leftVal);
        equation.setRightValue(rightVal);
        equation.setOpCode(opsCode);

        return equation;

    }
}
