package cucumberDemo;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FirstTest {

    private String today;
    private String actualAnswer;

    @Given("^today is Sunday$")
    public void today_is_sunday() {
        this.today = "Sunday";
    }

    @When("^I ask whether it's Friday yet$")
    public void verifyFriday() {
        this.actualAnswer = "Friday";
    }

    @Then("^I should be told \"Nope\"$")
    public void answerDay() {
        System.out.println("Today is not Friday, it is " +today);
    }
}
