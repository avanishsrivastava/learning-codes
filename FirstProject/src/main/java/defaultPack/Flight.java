package defaultPack;

public class Flight {

    private int passengers;
    private int seats;

    public Flight() {
        passengers = 0;
        seats = 150;
    }

    void addPassenger() {
        if(passengers < seats) {
            passengers += 1;
            seats -= 1;
        }
        else
            handleTooMany();
    }

    private void handleTooMany() {
        System.out.println("No more seats available");
    }

    void showSeats() {
        System.out.println("Seats remaining: " +seats);
    }

    void showPassengers() {
        System.out.println("Passengers booked: " +passengers);
    }

    public int getSeats() {
        return seats;
    }

    public int getPassengers() {
        return passengers;
    }

    void setSeats(int seats) {
        this.seats = seats;
    }

    void setPassengers(int passengers) {
        this.passengers = passengers;
    }




}
