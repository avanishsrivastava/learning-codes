package defaultPack;

public class ConditionalOperator {

    public static void main(String ar[]) {

        int v1 = 7;

        int v2 = 9;

        int result = (v1 > v2) ? v1 : v2;

//        if(v1 > v2)
//            result = v1;
//        else
//            result = v2;

        System.out.println("Greater value is: " + result);

        int students = 30;
        int rooms = 4;

        float studentsPerRoom = (rooms == 0.0f) ? 0.0f : (float) students / rooms;

        System.out.println("Students per room is: " + studentsPerRoom);

    }

}
