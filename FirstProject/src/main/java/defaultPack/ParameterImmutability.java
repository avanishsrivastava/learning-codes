package defaultPack;

public class ParameterImmutability {

    public static void main(String ar[]) {

        int i=10, j=20;

        System.out.println("Value of i: " + i);
        System.out.println("Value of j: " + j);

        swap(i,j);

        System.out.println("Value of i outside method: " + i);
        System.out.println("Value of j outside method: " + j);

    }

    static void swap(int i, int j) {
        int temp = i;
        i=j;
        j=temp;
        System.out.println("Value of i inside method: " + i);
        System.out.println("Value of j inside method: " + j);
    }

}
