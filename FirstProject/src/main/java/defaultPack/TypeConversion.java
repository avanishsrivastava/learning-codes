package defaultPack;

public class TypeConversion {

    public static void main(String ar[]) {

        byte varByte = 120;                  // Range from -128(2^8) to 127((2^8) - 1)
        short varShort = 32765;                // Range from -32768(2^16) to 32767((2^16) - 1)
        int varInt = 1324564696;                  // Range from (2^32) to ((2^32) - 1)
        long varLong = 132456469684541L;               // Range from (2^64) to ((2^64) - 1)
        float varFloat = 19022156.23f;             // Range from (2^32) to ((2^32) - 1)
        double varDouble = 289374.28439d;       // Range from (2^64) to ((2^64) - 1)

        System.out.println(varByte);
        System.out.println(varShort);
        System.out.println(varInt);
        System.out.println(varLong);
        System.out.println(varFloat);
        System.out.println(varDouble);

        System.out.println("Implicit conversion shown below");

        //Implicit conversion
        varShort = varByte;
        varLong = varInt;
        varDouble = varFloat;

        System.out.println(varShort);
        System.out.println(varLong);
        System.out.println(varDouble);

        System.out.println("Explicit conversion shown below");

        varShort = (short) varInt;
        System.out.println(varShort);

        varFloat = (float) varDouble;
        System.out.println(varFloat);

    }

}
