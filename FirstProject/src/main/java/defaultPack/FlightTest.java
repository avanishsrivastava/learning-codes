package defaultPack;

public class FlightTest {

    public static void main(String ar[]) {

        Flight flight = new Flight();
        for(int i = 1; i<=10; i++)
        flight.addPassenger();

        flight.showSeats();
        flight.showPassengers();

        Flight flight1 = new Flight();
        for(int i = 1; i<=50; i++)
            flight1.addPassenger();

        flight1.showSeats();
        flight1.showPassengers();

        flight1 = flight;

        for(int i = 1; i<=15; i++)
        flight.addPassenger();

        flight1.showSeats();
        flight1.showPassengers();

        Flight flight2 = new Flight();

        flight2.showSeats();
        flight2.showPassengers();

        flight2.setSeats(95);
        flight2.setPassengers(80);

        System.out.println("Seats in defaultPack.Flight 2: " + flight2.getSeats());
        System.out.println("Passengers in defaultPack.Flight 2: " + flight2.getPassengers());

        flight2.showSeats();
        flight2.showPassengers();



    }

}
