package defaultPack;

import javaBasics.Constructor.FlightWithInitialBlock;

public class ParameterImmutabilityInClass {

    public static void main(String ar[]) {

        FlightWithInitialBlock i = new FlightWithInitialBlock(12546);
        FlightWithInitialBlock j = new FlightWithInitialBlock(65432);

        System.out.println("Value of defaultPack.Flight 1: " + i.getFlightNumber());
        System.out.println("Value of defaultPack.Flight 2: " + j.getFlightNumber());

        swapReferences(i,j);

        System.out.println("Value of defaultPack.Flight 1 outside method after reference swapping: " + i.getFlightNumber());
        System.out.println("Value of defaultPack.Flight 2 outside method after reference swapping: " + j.getFlightNumber());

        swapValues(i,j);
        System.out.println("Value of defaultPack.Flight 1 outside method after value swapping: " + i.getFlightNumber());
        System.out.println("Value of defaultPack.Flight 2 outside method after value swapping: " + j.getFlightNumber());

    }

    static void swapReferences(FlightWithInitialBlock i, FlightWithInitialBlock j) {

        FlightWithInitialBlock temp = i;
        i=j;
        j=temp;

        System.out.println("Value of defaultPack.Flight 1 inside method: " + i.getFlightNumber());
        System.out.println("Value of defaultPack.Flight 2 inside method: " + j.getFlightNumber());

    }

    static void swapValues(FlightWithInitialBlock i, FlightWithInitialBlock j) {

        int k = i.getFlightNumber();
        i.setFlightNumber(j.getFlightNumber());
        j.setFlightNumber(k);

        System.out.println("Value of defaultPack.Flight 1 inside method: " + i.getFlightNumber());
        System.out.println("Value of defaultPack.Flight 2 inside method: " + j.getFlightNumber());

    }

}
