package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FirstTest {

    private int sum = 0;


    @Given("^User should be on the main page of amazon$")
    public void user_on_home_page() {
        System.out.println("Writing the code");
    }

    @When("^user will navigate to login page and enter valid credentials$")
    public void doLogin() {
        System.out.println("Redirecting to login page");

    }

    @Then("^user should be landed on the main page$")
    public void doLogout() {
        System.out.println("Logout");

    }

    @When("^user will enter \"([^\"]*)\" and \"([^\"]*)\"$")
    public void sumOfNumbers(String num1, String num2) {
        sum = Integer.parseInt(num1) + Integer.parseInt(num2);
//        System.out.println("Sum of numbers: " + sum);
    }

    @Then("^sum of numbers should be printed on console$")
    public void print_sum() {
        System.out.println("\n" + sum);
    }
}
