Feature: Testing scenarios using cucumber

  Scenario: The user will enter two positive numbers and the result should be printed on console
    When user will enter "10" and "15"
    Then sum of numbers should be printed on console

  Scenario: The user should be able to login with valid credentials and should be able to logout
    Given User should be on the main page of amazon
    When user will navigate to login page and enter valid credentials
    Then user should be landed on the main page

  Scenario Outline: The user will enter two positive numbers as parameters and the result should be printed on console
    When user will enter "<val1>" and "<val2>"
    Then sum of numbers should be printed on console
    Examples:
      | val1 | val2 |
      | 10   | 15   |
      | 20   | 35   |